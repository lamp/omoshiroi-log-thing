var express = require("express");
var fs = require("fs");
var escapeHtml = require("escape-html");

var app = express();
app.set("trust proxy", "127.0.0.1");
app.listen(8080, "127.0.0.1");

var html_top = fs.readFileSync("template.html", "utf8");

app.use((req, res, next) => {
	let url = `${req.protocol}://${req.hostname}${req.url}`;
	let ua = req.headers["user-agent"] && escapeHtml(req.headers["user-agent"]);
	let referer = req.headers['referer'] && escapeHtml(req.headers['referer']);
	let line =
		`<span class="date">[${new Date().toISOString()}]</span> <span class="ip"><a href="https://whatismyipaddress.com/ip/${req.ip}" target="_blank">${req.ip}</a></span> ` +
		`<span class="tlsver">${req.headers["x-forwarded-tlsversion"]||'-'}</span> ` +
		`<span class="httpver">${req.headers["x-forwarded-realproto"]||req.httpVersion}</span> ` +
		`<span class="method">${req.method}</span> <span class="url"><a href="${encodeURI(url)}" target="_blank">${escapeHtml(url)}</a></span>` +
		`	<span class="ua">${ua ? `"${ua}"` : '-'}</span> ` +
		`<span class="referrer">${referer ? `<a href="${referer}" target="_blank">${referer}</a>` : '-'}</span>`
	;
	fs.appendFileSync("log.html", line + '\n');
	next();
});

app.use((req, res, next) => {
	if (req.protocol != "https" || req.hostname != "c-73-189-133-150.hsd1.ca.comcast.net") {
		res.redirect("https://c-73-189-133-150.hsd1.ca.comcast.net/");
	} else next();
});

app.get("/", (req, res) => {
	res.write(html_top);
	fs.createReadStream("log.html").pipe(res);
});

app.use(express.static(process.cwd()));

app.get("*", (req, res) => {
	res.redirect('/');
});
